﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advanced11_first_time_homework_DBInterface
{
    public interface DBHelper
    {
        DbConnection CreateDBConnection();
        string ConnectionStr { get;}
        int Execute(string sql, SqlParameter[] sqlParameters = null);
        SqlDataReader QueryDataReader(string sql, DbConnection sqlConnection, SqlParameter[] sqlParameters = null);
    }
}
