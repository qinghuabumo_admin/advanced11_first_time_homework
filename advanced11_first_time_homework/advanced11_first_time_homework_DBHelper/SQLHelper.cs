﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using advanced11_first_time_homework_DBInterface;

namespace advanced11_first_time_homework_DBHelper
{

    public class SQLHelper : advanced11_first_time_homework_DBInterface.DBHelper
    {
        public string ConnectionStr { get; } = ConfigurationManager.AppSettings["connectionStr"];
        
        public int Execute(string sql, SqlParameter[] sqlParameters = null)
        {
            int reponseValue = 0;
            using (SqlConnection conn = new SqlConnection(ConnectionStr))
            {
                SqlCommand sqlCommand = new SqlCommand(sql, conn);
                if (sqlParameters != null)
                {
                    sqlCommand.Parameters.AddRange(sqlParameters);
                }
                conn.Open(); // 打开数据库连接
                try
                {
                    reponseValue = sqlCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sqlCommand.Dispose();
            }
            return reponseValue;
        }
        public DbConnection CreateDBConnection()
        {
            return new SqlConnection(ConnectionStr);
        }
        public SqlDataReader QueryDataReader(string sql,DbConnection Connection,SqlParameter[] sqlParameters = null)
        {
            SqlDataReader dr = null;
            SqlCommand sqlCommand = new SqlCommand(sql, (SqlConnection)Connection);
            if (sqlParameters != null)
            {
                sqlCommand.Parameters.AddRange(sqlParameters);
            }
            Connection.Open(); // 打开数据库连接
            try
            {
                dr = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dr;
        }

    }
}
