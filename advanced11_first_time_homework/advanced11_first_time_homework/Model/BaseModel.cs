﻿using advanced11_first_time_homework.CustomerAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advanced11_first_time_homework.Model
{
    public class BaseModel
    {
        [Key]
        [Required]
        public int Id { get; set; }

    }
}
