﻿using advanced11_first_time_homework.CustomerAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advanced11_first_time_homework.Model
{
    public class Company: BaseModel
    {
        //       [Id] [int] IDENTITY(1,1) NOT NULL,

        //   [Name] [nvarchar] (500) NULL,
        //[CreateTime]
        //       [datetime]
        //       NOT NULL,

        //   [CreatorId] [int] NOT NULL,

        //   [LastModifierId] [int] NULL,
        //[LastModifyTime] [datetime] NULL,
        [Table(SqlPropertyName = "Name")]
        public string MyName { get; set; }
        public DateTime Createtime { get; set; }
        public int CreatorId { get; set; }
        public int LastModifierId { get; set; }
        public DateTime LastModifyTime { get; set; }
    }
}
