﻿using advanced11_first_time_homework.CustomerAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advanced11_first_time_homework.Model
{
    [Table(SqlTableName = "User")]
    public class User:BaseModel
    {
        [StringLength(MaxLength = 2)]
        [Table(SqlPropertyName = "Name", DisplayName = "名字")]
        public string Name { get; set; }
        public string Account { get; set; }
        [Table(SqlPropertyName = "Password", DisplayName = "密码")]
        public string Password1 { get; set; }
        [Table(DisplayName = "邮箱")]
        public string Email { get; set; }
        [MobileValidate(@"^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$")]
        [StringLength(MaxLength =11,MinLength = 11)]
        public string Mobile { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int State { get; set; }
        public int UserType { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public DateTime CreateTime { get; set; }
        public int CreatorId { get; set; }
        public int LastModifierId { get; set; }
        public DateTime? LastModifyTime { get; set; }
        //[Id][int] IDENTITY(1,1) NOT NULL,

        //[Name] [nvarchar] (50) NULL,
        //	[Account] [varchar] (100) NOT NULL,

        //     [Password] [varchar] (100) NOT NULL,

        //      [Email] [varchar] (200) NULL,
        //	[Mobile] [varchar] (30) NULL,
        //	[CompanyId] [int] NULL,
        //	[CompanyName] [nvarchar] (500) NULL,
        //	[State] [int] NOT NULL,

        //    [UserType] [int] NOT NULL,

        //    [LastLoginTime] [datetime] NULL,
        //	[CreateTime]
        //        [datetime]
        //        NOT NULL,

        //    [CreatorId] [int] NOT NULL,

        //    [LastModifierId] [int] NULL,
        //	[LastModifyTime] [datetime] NULL,
    }
}
