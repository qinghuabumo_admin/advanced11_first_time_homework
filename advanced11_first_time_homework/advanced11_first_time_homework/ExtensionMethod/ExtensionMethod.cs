﻿using advanced11_first_time_homework.CustomerAttribute;
using advanced11_first_time_homework.Model;
using advanced11_first_time_homework.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace advanced11_first_time_homework.ExtensionMethod
{
    public static class ExtensionMethod
    {
        public static string GetTableName(this BaseModel oThis)
        {
            string tableName = "";
            var oThisType = oThis.GetType();
            Attribute attribute = Attribute.GetCustomAttribute(oThisType, typeof(TableAttribute));
            if (attribute != null)
            {
                System.Reflection.PropertyInfo TableNameProp = attribute.GetType().GetProperty("TableName");
                if (TableNameProp != null)
                {
                    tableName = TableNameProp.GetValue(attribute)?.ToString();
                }
            }
            if (string.IsNullOrEmpty(tableName))
            {
                tableName = oThisType.Name;
            }
            return tableName;
        }
        public static List<VM_NameValue> GetModelValue(this BaseModel oThis)
        {
            List<VM_NameValue> list = new List<VM_NameValue>();
            var type = oThis.GetType();
            foreach (var item in type.GetProperties())
            {

                var value = item.GetValue(oThis);
                if (value != null && !value.GetType().Equals(typeof(DBNull)))
                {
                    list.Add(new VM_NameValue() { Name = item.GetSqlPropertyDisplayName(), Value = value.ToString() });
                }
                else
                {
                    list.Add(new VM_NameValue() { Name = item.GetSqlPropertyDisplayName(), Value = null });
                }
            }

            return list;
        }
        public static string GetSqlPropertyName(this BaseModel oThis)
        {
            Type type = oThis.GetType();
            foreach (var item in type.GetProperties())
            {
                string name = "";
                if (item.IsDefined(typeof(TableAttribute), true))
                {
                    TableAttribute tableAttribute = (TableAttribute)Attribute.GetCustomAttribute(item, typeof(TableAttribute));
                    name = tableAttribute?.SqlPropertyName;
                }
                else
                {
                    name = item.Name;
                }
                Console.WriteLine(name);
            }
            return "";
        }
        public static string GetQuerySqlString(this BaseModel oThis)
        {
            Type type = oThis.GetType();
            List<string> liststring = new List<string>();
            foreach (var item in type.GetProperties())
            {
                string name = "";
                if (item.IsDefined(typeof(TableAttribute), true))
                {
                    TableAttribute tableAttribute = (TableAttribute)Attribute.GetCustomAttribute(item, typeof(TableAttribute));
                    name = tableAttribute?.SqlPropertyName;
                }
                if (string.IsNullOrEmpty(name))
                {
                    name = item.Name;
                }
                liststring.Add(name);
            }
            string sql = $"select {string.Join(",", liststring.Select(p => $"[{p}]"))} from [{oThis.GetTableName()}]";
            return sql;
        }
        public static string GetSqlPropertyName(this PropertyInfo oThis)
        {
            string name = "";
            if (oThis.IsDefined(typeof(TableAttribute)))
            {
                TableAttribute tableAttribute = (TableAttribute)oThis.GetCustomAttribute(typeof(TableAttribute));
                name = tableAttribute?.SqlPropertyName;
            }
            if (string.IsNullOrEmpty(name))
            {
                name = oThis.Name;
            }
            return name;
        }
        public static string GetSqlPropertyValue(this PropertyInfo oThis)
        {
            string name = "";
            object ob = oThis.GetValue(oThis);
            return name;
        }
        public static string GetSqlPropertyDisplayName(this PropertyInfo oThis)
        {
            string name = "";
            if (oThis.IsDefined(typeof(TableAttribute)))
            {
                TableAttribute tableAttribute = (TableAttribute)oThis.GetCustomAttribute(typeof(TableAttribute));
                name = tableAttribute?.DisplayName;
            }
            if (string.IsNullOrEmpty(name))
            {
                name = oThis.Name;
            }
            return name;
        }
        public static bool PropertyIsKey(this PropertyInfo oThis)
        {
            if (oThis.IsDefined(typeof(KeyAttribute)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
