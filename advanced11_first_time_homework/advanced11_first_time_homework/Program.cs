﻿using advanced11_first_time_homework.CustomerAttribute;
using advanced11_first_time_homework.CustomerMethod;
using advanced11_first_time_homework.DBHelper;
using advanced11_first_time_homework.ExtensionMethod;
using advanced11_first_time_homework.Factory;
using advanced11_first_time_homework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advanced11_first_time_homework
{
    class Program
    {
        static void Main(string[] args)
        {
            //foreach (var item in DBHelper.DBHelper.GetModelById<User>(1).GetModelValue())
            //{
            //    Console.WriteLine($"{item.Name} ={item.Value }");
            //}
            //Console.WriteLine("----------------------------------------------------------------------");
            //DBHelper.DBHelper.GetList<User>().ForEach(p=>{
            //    foreach (var item in p.GetModelValue())
            //    {
            //        Console.WriteLine($"{item.Name} ={item.Value }");
            //    }
            //    Console.WriteLine("----------------------------------------------------------------------");
            //});
            //foreach (var item in DBHelper.DBHelper.GetModelById<Company>(1).GetModelValue())
            //{
            //    Console.WriteLine($"{item.Name} ={item.Value }");
            //}
            //Console.WriteLine("----------------------------------------------------------------------");
            //DBHelper.DBHelper.GetList<Company>().ForEach(p => {
            //    foreach (var item in p.GetModelValue())
            //    {
            //        Console.WriteLine($"{item.Name} ={item.Value }");
            //    }
            //    Console.WriteLine("----------------------------------------------------------------------");
            //});
            //Console.WriteLine("-------------------------添加一个用户--------------------------------");
            User user = new User();
            Console.WriteLine("请输入名称");
            user.Name = Console.ReadLine();
            Console.WriteLine("请输入账号");
            user.Account = Console.ReadLine();
            Console.WriteLine("请输入密码");
            user.Password1 = Console.ReadLine();
            user.State = 0;
            user.UserType = 2;
            user.CreatorId = 2;
            user.CreateTime = DateTime.Now;
            Console.WriteLine("处理---返回结果为：" + DBHelper.DBHelper.AddByModel(user));
            Console.WriteLine("----------------------------------------------------------------------");

            //Console.WriteLine("-------------------------修改一个用户的姓名--------------------------------");
            //User user1 = DBHelper.DBHelper.GetModelById<User>(1);
            //Console.WriteLine($"当前用户名称为：{user1.Name} 请输入新的名称");
            //user1.Name = Console.ReadLine();
            //Console.WriteLine("处理---返回结果为：" + DBHelper.DBHelper.UpdateByModel(user1));
            //Console.WriteLine("----------------------------------------------------------------------");

            //Console.WriteLine("-------------------------删除一个用户--------------------------------");
            //Console.WriteLine($"请输入要删除的用户的id");
            //Console.WriteLine("处理---返回结果为：" + DBHelper.DBHelper.DeleteByModel<User>(int.Parse(Console.ReadLine())));
            //Console.WriteLine("----------------------------------------------------------------------");


            Console.ReadKey();
        }
    }
}
