﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace advanced11_first_time_homework.CustomerAttribute
{
    [AttributeUsage(AttributeTargets.Property)]
    public abstract class ValidateAttribute : Attribute
    {
        public abstract string GetMsg();
        protected abstract string Msg { get; set; }
        public abstract bool Validate<T>(PropertyInfo prop,T model);
    }
    /// <summary>
    /// 不可为空
    /// </summary>
    public class RequiredAttribute : ValidateAttribute
    {
        protected override string Msg { get; set; } = "非空验证失败";

        public override string GetMsg()
        {
            return Msg;
        }

        public override bool Validate<T>(PropertyInfo prop,T model)
        {
            string value =  prop.GetValue(model)?.ToString();
            return string.IsNullOrEmpty(value);
        }
    }
    public class StringLengthAttribute : ValidateAttribute
    {
        public override string GetMsg()
        {
            return Msg;
        }
        protected override string Msg { get; set; } = "长度验证失败";
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        public override bool Validate<T>(PropertyInfo prop,T model)
        {
            StringLengthAttribute stringLengthAttribute = prop.GetCustomAttribute<StringLengthAttribute>();
            object obj = prop.GetValue(model);
            if( obj == null)
            {
                return false;
            }
            string value = obj.ToString();
            if((stringLengthAttribute.MinLength==0 || stringLengthAttribute.MinLength<=value.Length) && (stringLengthAttribute.MaxLength == 0 || stringLengthAttribute.MaxLength >= value.Length))
            {
                return true;
            }
            return false;
        }
    }
    public class EmailValidateAttribute : ValidateAttribute
    {
        public override string GetMsg()
        {
            return Msg;
        }
        protected override string Msg { get; set; } = "邮箱格式不正确";
        private string RegularExpression { get; set; }

        public EmailValidateAttribute(string RegularExpression)
        {
            this.RegularExpression = RegularExpression;
        }
        public override bool Validate<T>(PropertyInfo prop, T model)
        {
            object obj = prop.GetValue(model);
            if (obj == null)
            {
                return false;
            }
            string value = obj.ToString();
            return Regex.IsMatch(value, RegularExpression);
        }
    }
    public class MobileValidateAttribute : ValidateAttribute
    {
        public override string GetMsg()
        {
            return Msg;
        }
        protected override string Msg { get; set; } = "手机格式不正确";
        private string RegularExpression { get; set; }

        public MobileValidateAttribute(string RegularExpression)
        {
            this.RegularExpression = RegularExpression;
        }
        public override bool Validate<T>(PropertyInfo prop, T model)
        {
            object obj = prop.GetValue(model);
            if (obj == null)
            {
                return false;
            }
            string value = obj.ToString();
            return Regex.IsMatch(value, RegularExpression);
        }
    }

}
