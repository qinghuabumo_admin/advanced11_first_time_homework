﻿using advanced11_first_time_homework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace advanced11_first_time_homework.CustomerAttribute
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class TableAttribute : Attribute
    {
        public string SqlTableName { get; set; }
        public string SqlPropertyName { get; set; }
        public string DisplayName { get; set; }

    }
}
