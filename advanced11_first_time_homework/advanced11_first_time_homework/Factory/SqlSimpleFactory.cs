﻿using advanced11_first_time_homework.DBHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using advanced11_first_time_homework_DBInterface;
using System.Configuration;

namespace advanced11_first_time_homework.Factory
{
    public class SqlSimpleFactory
    {
        static string DBHelperConfig = ConfigurationManager.AppSettings["DBHelperConfig"].ToString();
        public static advanced11_first_time_homework_DBInterface.DBHelper createDBHelper()
        {
            string dllfile = DBHelperConfig.Split(',')[0];
            string typename = DBHelperConfig.Split(',')[1];
            advanced11_first_time_homework_DBInterface.DBHelper sQLHelper = (advanced11_first_time_homework_DBInterface.DBHelper)Assembly.Load(dllfile).CreateInstance(typename);
            return sQLHelper;
        }
    }
}
