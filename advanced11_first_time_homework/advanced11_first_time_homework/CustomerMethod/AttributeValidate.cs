﻿using advanced11_first_time_homework.CustomerAttribute;
using advanced11_first_time_homework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace advanced11_first_time_homework.CustomerMethod
{
    /// <summary>
    /// 特性验证
    /// </summary>
    public static class AttributeValidate
    {
        /// <summary>
        /// 特性验证 返回错误列表 如果列表count为0 则没错
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<ValudateErrorModel> Validate<T>(T model) where T: BaseModel
        {
            List<ValudateErrorModel> errorList = new List<ValudateErrorModel>();
            Type type = model.GetType();
            foreach (var item in type.GetProperties())
            {
                foreach (var item1 in item.GetCustomAttributes<ValidateAttribute>())
                {
                    if (!item1.Validate<T>(item, model))
                    {
                        errorList.Add(new ValudateErrorModel() { Error = item1.GetMsg(), Prop = item, PropertyName = item.Name });
                    };
                }
                
            }
            return errorList;
        }
    }
    public class ValudateErrorModel
    {
        public PropertyInfo Prop { get; set; }
        public String Error { get; set; }
        public String PropertyName { get; set; }
    }

}
