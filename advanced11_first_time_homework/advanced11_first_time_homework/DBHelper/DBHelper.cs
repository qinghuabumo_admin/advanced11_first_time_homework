﻿using advanced11_first_time_homework.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using advanced11_first_time_homework.CustomerAttribute;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using advanced11_first_time_homework.ExtensionMethod;
using advanced11_first_time_homework.Factory;
using advanced11_first_time_homework_DBInterface;
using System.Data.Common;
using advanced11_first_time_homework.CustomerMethod;

namespace advanced11_first_time_homework.DBHelper
{
    public static class DBHelper
    {
        static advanced11_first_time_homework_DBInterface.DBHelper dBHelper = SqlSimpleFactory.createDBHelper();
        public static T GetModelById<T>(int id) where T : BaseModel, new()
        {
            T t = new T();
            String sql = $"select {string.Join(",", t.GetType().GetProperties().Select(p => $"[{p.GetSqlPropertyName()}]"))} from [{t.GetTableName()}] where id = @id"; // 
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@id", id));
            DbConnection dbConnection = dBHelper.CreateDBConnection();
            SqlDataReader dr = dBHelper.QueryDataReader(sql, dbConnection, sqlParameters.ToArray());
            if (dr?.Read() != null)
            {
                var type = t.GetType();
                foreach (var item in type.GetProperties())
                {
                    item.SetValue(t, dr[item.GetSqlPropertyName()]);
                }
            }
            dbConnection.Dispose();
            return t;
        }
        public static int AddByModel<T>(T model) where T : BaseModel
        {
            List<ValudateErrorModel> valudateErrorModels = AttributeValidate.Validate(model);
            if (valudateErrorModels.Count > 0)
            {
                foreach (var item in valudateErrorModels)
                {
                    Console.WriteLine(item.PropertyName+":"+item.Error);
                }
                return 0;
            }
            T t = Activator.CreateInstance<T>();
            Type type = t.GetType();
            string sql = $"INSERT INTO [{t.GetTableName()}] ({string.Join(",", type.GetProperties().Where(p => !p.PropertyIsKey()).Select(p => $"[{p.GetSqlPropertyName()}]"))}) VALUES ({string.Join(",", type.GetProperties().Where(p => !p.PropertyIsKey()).Select(p => p.GetValue(model) == null ? $"null" : $"'{p.GetValue(model)}'"))})";
            return dBHelper.Execute(sql);
        }
        public static int UpdateByModel<T>(T model) where T : BaseModel
        {
            List<ValudateErrorModel> valudateErrorModels = AttributeValidate.Validate(model);
            if (valudateErrorModels.Count > 0)
            {
                foreach (var item in valudateErrorModels)
                {
                    Console.WriteLine(item.PropertyName + ":" + item.Error);
                }
                return 0;
            }
            T t = Activator.CreateInstance<T>();
            Type type = t.GetType();
            string sql = $"Update [{t.GetTableName()}]  SET {string.Join(",", type.GetProperties().Where(p =>!p.PropertyIsKey()).Select(p => $"[{p.GetSqlPropertyName()}]='{p.GetValue(model)}'"))} where id = @id";
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@id", model.Id));
            return dBHelper.Execute(sql,sqlParameters.ToArray());
        }
        public static int DeleteByModel<T>(int id) where T : BaseModel
        {
            T t = Activator.CreateInstance<T>();
            Type type = t.GetType();
            string sql = $"Delete from [{t.GetTableName()}] where id = @id";
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@id", id));
            return dBHelper.Execute(sql, sqlParameters.ToArray());
        }
        public static List<T> GetList<T>() where T : BaseModel,new()
        {
            List<T> list = new List<T>();
            T t1 = new T();
            String sql = $"select {string.Join(",", typeof(T).GetProperties().Select(p => $"[{p.GetSqlPropertyName()}]"))} from [{t1.GetTableName()}]";
            DbConnection dbConnection = dBHelper.CreateDBConnection();
            SqlDataReader dr = dBHelper.QueryDataReader(sql, dbConnection);
            while (dr.Read())
            {
                T t = new T();
                foreach (var item in t.GetType().GetProperties())
                {
                    var value = dr[item.GetSqlPropertyName()];
                    if (value != null && !value.GetType().Equals(typeof(DBNull)))
                    {
                        item.SetValue(t, value);
                    }
                }
                list.Add(t);
            }
            return list;
        }
    }
}
